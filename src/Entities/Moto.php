<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Moto
 *
 * @ORM\Table(name="Moto")
 * @ORM\Entity
 */
class Moto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_moto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMoto;

    /**
     * @var string
     *
     * @ORM\Column(name="marque", type="text", length=65535, nullable=false)
     */
    private $marque;

    /**
     * @var string
     *
     * @ORM\Column(name="modele", type="text", length=65535, nullable=false)
     */
    private $modele;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="text", length=65535, nullable=false)
     */
    private $etat;

    /**
     * @var int
     *
     * @ORM\Column(name="kilometrage", type="integer", nullable=false)
     */
    private $kilometrage;

    /**
     * @var int
     *
     * @ORM\Column(name="id_agence", type="integer", nullable=false)
     */
    private $idAgence;

    /**
     * @var bool
     *
     * @ORM\Column(name="disponible", type="boolean", nullable=false)
     */
    private $disponible;


}
