<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="Location", indexes={@ORM\Index(name="Location_Moto1_FK", columns={"id_moto"}), @ORM\Index(name="Location_Utilisateur_FK", columns={"id_utilisateur"}), @ORM\Index(name="Location_Agence0_FK", columns={"id_agence"})})
 * @ORM\Entity
 */
class Location
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_location", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLocation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=false)
     */
    private $dateFin;

    /**
     * @var int
     *
     * @ORM\Column(name="kilometres_parcouru", type="integer", nullable=false)
     */
    private $kilometresParcouru;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var bool
     *
     * @ORM\Column(name="statut", type="boolean", nullable=false)
     */
    private $statut;

    /**
     * @var \Agence
     *
     * @ORM\ManyToOne(targetEntity="Agence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agence", referencedColumnName="id_agence")
     * })
     */
    private $idAgence;

    /**
     * @var \Moto
     *
     * @ORM\ManyToOne(targetEntity="Moto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_moto", referencedColumnName="id_moto")
     * })
     */
    private $idMoto;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;


}
